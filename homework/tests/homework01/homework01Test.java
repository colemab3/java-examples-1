package homework01;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class homework01Test {

    @org.junit.jupiter.api.Test
    void testSum() {
        int[] nums = {-5, 234, -23};
        homework01 myWork = new homework01();
        assertEquals(206, myWork.sum(nums));
        assertEquals(myWork.sumAgain(nums), myWork.sum(nums));
        assertEquals(0, myWork.sum());
    }

    @org.junit.jupiter.api.Test
    void testFailSum(){
        homework01 myWork = new homework01();
        assertEquals(4, myWork.sum(4,5,6,7));
    }
}