package module01;

/*
    Brody Coleman
    Whatcom Community College
    CS145
    09/24/2018

    The beginning of review of java.
 */

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        System.out.print("How many repeats? :");
        int quantity = in.nextInt();

        for (int i = 0; i < quantity; i++){
            System.out.println(sayThing("greetings, Hello!", i));
        }
        System.out.println("Good bye.");
    }

    /**
     * sayThing is a simple method that uses String format to put a string in order.
     *
     * @param saying the words to repeat
     * @param index line number
     * @return formatted string
     */
    public static String sayThing(String saying, int index){
        return String.format("%2d. %s\n", index, saying);
    }
}
